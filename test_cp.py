from copy import deepcopy
d = {}
d['names'] = ['Alfred','Bertrand']
c = d.copy()
dc = deepcopy(d)
d['names'].append('Clive')
print(c)
print(dc)

e = {}.fromkeys(['name','age'])
print(e)

f = dict.fromkeys(['name','age'])
print(f)

print(f.get('name'))